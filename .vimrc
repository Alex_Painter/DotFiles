set nocompatible
filetype off

" Plugins:

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'kien/ctrlp.vim'

Plugin 'scrooloose/syntastic'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-fireplace'
Plugin 'tpope/vim-commentary'
Plugin 'pangloss/vim-javascript'
Plugin 'klen/python-mode'
Plugin 'Townk/vim-autoclose'
Plugin 'maksimr/vim-jsbeautify'
Plugin 'morhetz/gruvbox'

" React
Plugin 'mxw/vim-jsx'

"Plugin 'christoomey/vim-tmux-navigator'

call vundle#end()
syntax on
filetype plugin indent on
filetype plugin on

" Settings:
set number
set relativenumber

set showcmd
set noswapfile
set autoread

set incsearch
set hlsearch

set nocursorcolumn
set nocursorline

set wrap

set tabstop=4
set shiftwidth=4
set expandtab

set smarttab

colorscheme gruvbox
set background=dark

" Status bar
"set laststatus=2
"set statusline=
"set statusline+=\ %*
"set statusline+=%1*\ ‹‹
"set statusline+=%1*\ %f\ %*
"set statusline+=%1*\ ››
"set statusline+=%1*\ %m
"set statusline+=%3*\ %F
"set statusline=%=
"set statusline+=%=%1*%y%*%*
"set statusline+=%10((%l,%c)%)\  
"set statusline+=%P
"hi User1 guifg=#d40000 guibg=#191f26

set laststatus=2
set statusline=
set statusline+=%2*\ %l
set statusline+=\ %*
set statusline+=%1*\ ‹‹
set statusline+=%1*\ %f\ %*
set statusline+=%1*\ ››
set statusline+=%1*\ %m
set statusline+=%3*\ %F
set statusline+=\ %{fugitive#statusline()}
set statusline+=%=
set statusline+=%3*\ ‹‹
set statusline+=%3*\ %{strftime('%R',getftime(expand('%')))}
set statusline+=%3*\ ::
set statusline+=%3*\ %n
set statusline+=%3*\ ››\ %*
"hi User1 guifg=#FFFFFF guibg=#191f26 gui=BOLD
"hi User2 guifg=#000000 guibg=#959ca6
"hi User3 guifg=#000000 guibg=#4cbf99


" Autmoatically open NerdTree if no file specified when starting vim
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
map <C-n> :NERDTreeToggle<CR>

" Key bindings:

imap jj <esc> 

nmap <C-j> <C-d>
nmap <C-k> <C-u>

" Navigating Vim tabs using j+k
nnoremap tk  :tabnext<CR>
nnoremap tj  :tabprev<CR>

" map <c-f> :call JsBeautify()<cr>
"" or
autocmd FileType javascript noremap <buffer>  <c-f> :call JsBeautify()<cr>
" for json
autocmd FileType json noremap <buffer> <c-f> :call JsonBeautify()<cr>
" " for jsx
autocmd FileType jsx noremap <buffer> <c-f> :call JsxBeautify()<cr>
" " for html
autocmd FileType html noremap <buffer> <c-f> :call HtmlBeautify()<cr>
" " for css or scss
autocmd FileType css noremap <buffer> <c-f> :call CSSBeautify()<cr>
